# SiLA 2 Server for Waveshare Barcode Scanner
This is an unofficial SiLA 2 server implementation for the [Waveshare Barcode Scanner Module](https://www.waveshare.com/barcode-scanner-module.htm).

## Requirements
- Python >= 3.7
- [Waveshare Barcode Scanner Module](https://www.waveshare.com/barcode-scanner-module.htm)
- Micro USB cable

## Installation
Download the project as zip file [here](https://gitlab.gwdg.de/umg-pharma-lab-automation/waveshare-scanner-sila2/-/archive/master/waveshare-scanner-sila2-master.zip) and install it with `pip install waveshare-scanner-sila2-master.zip`.

## Device setup
1. Use the Micro USB cable to connect the scanner to your machine
    - wait a few seconds until it beeps
2. While pressing the small button on the device, scan the QR code "USB Virtual Port" to enable device control via the USB cable
    - wait a few seconds until it beeps

![QR Code: USB Virtual Port](qr-usb-virtual-port.png)

## Determine port
- Windows
  - Open the device manager
  - Expand section "Ports (COM & LPT)"
  - Find entry like "USB Serial Device (COM3)", can also be "COM5" or any other number
- Linux
  - Usually it is "/dev/ttyACM0"

## Usage
To start the server, use `python -m ws_barcode_scanner_sila2 --serial-port COM5` (or `COM3` or `/dev/ttyACM0`).
This will launch the server on 127.0.0.1:50052, encrypted with a self-signed certificate.

Optional parameters:
- `--ip-address ADDRESS`, `--port PORT`": specify where the server should be launched
- `--ca-export-file FILE`: write the auto-generated CA certificate to the given file
- `--cert-file FILE`, `--private-key-file FILE`: use the provided credentials to encrypt the connection
- `--insecure`: use unencrypted connection
- `--verbose`: increase log verbosity

Run `python -m ws_barcode_scanner_sila2 --help` to receive a full list of available parameters.
