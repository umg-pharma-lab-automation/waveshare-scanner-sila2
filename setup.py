from setuptools import find_packages, setup

setup(
    name="ws_barcode_scanner_sila2",
    version="1.0.0",
    author="Niklas Mertsch",
    author_email="niklas.mertsch@stud.uni-goettingen.de",
    packages=find_packages(),
    install_requires=["sila2", "ws-barcode-scanner"],
    include_package_data=True,
)
