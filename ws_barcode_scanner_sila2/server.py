from typing import Optional
from uuid import UUID

from sila2.server import SilaServer

from .generated.barcodescanner import BarcodeScannerFeature
from .feature_implementations.barcodescanner_impl import BarcodeScannerImpl


class Server(SilaServer):
    def __init__(self, serial_port: str, server_uuid: Optional[UUID] = None):
        super().__init__(
            server_name="Waveshare Barcode Scanner",
            server_type="BarcodeScanner",
            server_version="0.1",
            server_description="Waveshare Barcode Scanner Module",
            server_vendor_url="https://gitlab.gwdg.de/umg-pharma-lab-automation/waveshare-scanner-sila2",
            server_uuid=server_uuid,
        )

        self.set_feature_implementation(BarcodeScannerFeature, BarcodeScannerImpl(serial_port))
