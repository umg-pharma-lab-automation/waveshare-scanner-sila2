from __future__ import annotations

from datetime import datetime, timedelta
from threading import Event
import time
from typing import Any, Dict

from sila2.framework import FullyQualifiedIdentifier, CommandExecutionStatus
from sila2.server import ObservableCommandInstanceWithIntermediateResponses
from ws_barcode_scanner import BarcodeScanner

from ..generated.barcodescanner import BarcodeScannerBase, Busy, Cancelled, ScanTimedOut, NotScanning


class BarcodeScannerImpl(BarcodeScannerBase):
    __scanner: BarcodeScanner
    __scanning: bool

    def __init__(
            self, serial_port: str, target_light_on: bool = True, white_light_on: bool = True, beep_on: bool = True
    ) -> None:
        super().__init__()
        self.__scanner = BarcodeScanner(serial_port)
        self.__cancel_event = Event()
        self.__stop_scanning()

        self.SwitchTargetLight(target_light_on, metadata={})
        self.SwitchWhiteLight(white_light_on, metadata={})
        self.SwitchBeep(beep_on, metadata={})

    def ScanForCode(self, Timeout: float, *, metadata: Dict[FullyQualifiedIdentifier, Any]) -> str:
        self.__start_scanning()
        self.update_CurrentMode("SingleScan")

        try:
            end_time = datetime.now() + timedelta(seconds=Timeout)
            while not self.__cancel_event.is_set() and datetime.now() < end_time:
                for code in self.__scanner.query_for_codes():
                    return code.decode("ascii")  # return first code found

                time.sleep(0.1)

            if self.__cancel_event.is_set():
                raise Cancelled
            else:
                raise ScanTimedOut
        finally:
            self.__stop_scanning()

    def StopScanning(self, *, metadata: Dict[FullyQualifiedIdentifier, Any]) -> None:
        if not self.__scanning:
            raise NotScanning
        self.__stop_scanning()

    def SwitchWhiteLight(
            self, On: bool, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> None:
        self.__scanner.memory_map[0x0000, 4:6] = 0b01 if On else 0b00
        self.update_WhiteLightEnabled(On)

    def SwitchTargetLight(
            self, On: bool, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> None:
        self.__scanner.memory_map[0x0000, 2:4] = 0b01 if On else 0b00
        self.update_TargetLightEnabled(On)

    def SwitchBeep(self, On: bool, *, metadata: Dict[FullyQualifiedIdentifier, Any]) -> None:
        self.__scanner.memory_map[0x0000, 1] = On
        self.update_BeepEnabled(On)

    def ScanContinuously(
            self,
            *,
            metadata: Dict[FullyQualifiedIdentifier, Any],
            instance: ObservableCommandInstanceWithIntermediateResponses[str],
    ) -> None:
        instance.status = CommandExecutionStatus.running
        self.__start_scanning()
        self.update_CurrentMode("ContinuousScan")

        while not self.__cancel_event.is_set():
            for code in self.__scanner.query_for_codes():
                instance.send_intermediate_response(code.decode("ascii"))
            time.sleep(0.1)

        self.__stop_scanning()

    def stop(self):
        self.__stop_scanning()
        super().stop()

    def __start_scanning(self) -> None:
        if self.__scanning:
            raise Busy
        self.__cancel_event.clear()
        self.__scanning = True

        # read all codes (ensure to only return new codes)
        _ = self.__scanner.query_for_codes()

        # enable continuous scanning mode
        self.__scanner.memory_map[0x0000, 6:8] = 0b10

    def __stop_scanning(self) -> None:
        self.__cancel_event.set()
        self.__disable_scanning()
        self.__scanning = False
        self.update_CurrentMode("Off")

    def __disable_scanning(self) -> None:
        # enter command mode and disable scanning
        self.__scanner.memory_map[0x0000, 6:8] = 0b01  # command mode
        self.__scanner.memory_map[0x0002, 7] = 0  # scan off
