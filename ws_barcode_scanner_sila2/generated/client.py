from __future__ import annotations

from typing import TYPE_CHECKING

from sila2.client import SilaClient

from .barcodescanner import BarcodeScannerFeature, Busy, Cancelled, NotScanning, ScanTimedOut

if TYPE_CHECKING:

    from .barcodescanner import BarcodeScannerClient


class Client(SilaClient):

    BarcodeScanner: BarcodeScannerClient

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self._register_defined_execution_error_class(
            BarcodeScannerFeature.defined_execution_errors["ScanTimedOut"], ScanTimedOut
        )

        self._register_defined_execution_error_class(BarcodeScannerFeature.defined_execution_errors["Busy"], Busy)

        self._register_defined_execution_error_class(
            BarcodeScannerFeature.defined_execution_errors["Cancelled"], Cancelled
        )

        self._register_defined_execution_error_class(
            BarcodeScannerFeature.defined_execution_errors["NotScanning"], NotScanning
        )
