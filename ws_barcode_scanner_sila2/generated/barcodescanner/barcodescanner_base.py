from __future__ import annotations

from abc import ABC, abstractmethod
from queue import Queue
from typing import Any, Dict, Optional

from sila2.framework import FullyQualifiedIdentifier
from sila2.server import FeatureImplementationBase, ObservableCommandInstanceWithIntermediateResponses

from .barcodescanner_types import (
    ScanContinuously_IntermediateResponses,
    ScanContinuously_Responses,
    ScanForCode_Responses,
    StopScanning_Responses,
    SwitchBeep_Responses,
    SwitchTargetLight_Responses,
    SwitchWhiteLight_Responses,
)


class BarcodeScannerBase(FeatureImplementationBase, ABC):

    _CurrentMode_producer_queue: Queue[str]

    _WhiteLightEnabled_producer_queue: Queue[bool]

    _TargetLightEnabled_producer_queue: Queue[bool]

    _BeepEnabled_producer_queue: Queue[bool]

    def __init__(self):
        """
        Barcode scanner
        """

        self._CurrentMode_producer_queue = Queue()

        self._WhiteLightEnabled_producer_queue = Queue()

        self._TargetLightEnabled_producer_queue = Queue()

        self._BeepEnabled_producer_queue = Queue()

    def update_CurrentMode(self, CurrentMode: str, queue: Optional[Queue[str]] = None):
        """
        The current scanning mode

        This method updates the observable property 'CurrentMode'.
        """
        if queue is None:
            queue = self._CurrentMode_producer_queue
        queue.put(CurrentMode)

    def CurrentMode_on_subscription(self, *, metadata: Dict[FullyQualifiedIdentifier, Any]) -> Optional[Queue[str]]:
        """
        The current scanning mode

        This method is called when a client subscribes to the observable property 'CurrentMode'

        :param metadata: The SiLA Client Metadata attached to the call
        :return: Optional `Queue` that should be used for updating this property.
            If None, the default Queue will be used.
        """
        pass

    def update_WhiteLightEnabled(self, WhiteLightEnabled: bool, queue: Optional[Queue[bool]] = None):
        """
        True if the white light is enabled during scanning

        This method updates the observable property 'WhiteLightEnabled'.
        """
        if queue is None:
            queue = self._WhiteLightEnabled_producer_queue
        queue.put(WhiteLightEnabled)

    def WhiteLightEnabled_on_subscription(
        self, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> Optional[Queue[bool]]:
        """
        True if the white light is enabled during scanning

        This method is called when a client subscribes to the observable property 'WhiteLightEnabled'

        :param metadata: The SiLA Client Metadata attached to the call
        :return: Optional `Queue` that should be used for updating this property.
            If None, the default Queue will be used.
        """
        pass

    def update_TargetLightEnabled(self, TargetLightEnabled: bool, queue: Optional[Queue[bool]] = None):
        """
        True if the red target light is enabled during scanning

        This method updates the observable property 'TargetLightEnabled'.
        """
        if queue is None:
            queue = self._TargetLightEnabled_producer_queue
        queue.put(TargetLightEnabled)

    def TargetLightEnabled_on_subscription(
        self, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> Optional[Queue[bool]]:
        """
        True if the red target light is enabled during scanning

        This method is called when a client subscribes to the observable property 'TargetLightEnabled'

        :param metadata: The SiLA Client Metadata attached to the call
        :return: Optional `Queue` that should be used for updating this property.
            If None, the default Queue will be used.
        """
        pass

    def update_BeepEnabled(self, BeepEnabled: bool, queue: Optional[Queue[bool]] = None):
        """
        True if the scan indicator beep is enabled during scanning

        This method updates the observable property 'BeepEnabled'.
        """
        if queue is None:
            queue = self._BeepEnabled_producer_queue
        queue.put(BeepEnabled)

    def BeepEnabled_on_subscription(self, *, metadata: Dict[FullyQualifiedIdentifier, Any]) -> Optional[Queue[bool]]:
        """
        True if the scan indicator beep is enabled during scanning

        This method is called when a client subscribes to the observable property 'BeepEnabled'

        :param metadata: The SiLA Client Metadata attached to the call
        :return: Optional `Queue` that should be used for updating this property.
            If None, the default Queue will be used.
        """
        pass

    @abstractmethod
    def ScanForCode(self, Timeout: float, *, metadata: Dict[FullyQualifiedIdentifier, Any]) -> ScanForCode_Responses:
        """
        Scan for a barcode


        :param Timeout: Timeout in seconds

        :param metadata: The SiLA Client Metadata attached to the call

        :return:

            - Barcode: The scanned code


        """
        pass

    @abstractmethod
    def StopScanning(self, *, metadata: Dict[FullyQualifiedIdentifier, Any]) -> StopScanning_Responses:
        """
        Stop current scan


        :param metadata: The SiLA Client Metadata attached to the call

        """
        pass

    @abstractmethod
    def SwitchWhiteLight(
        self, On: bool, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> SwitchWhiteLight_Responses:
        """
        Switch the white light on or off


        :param On: If true, the light will be on during scanning

        :param metadata: The SiLA Client Metadata attached to the call

        """
        pass

    @abstractmethod
    def SwitchTargetLight(
        self, On: bool, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> SwitchTargetLight_Responses:
        """
        Switch the red target light on or off


        :param On: If true, the target light will be on during scanning

        :param metadata: The SiLA Client Metadata attached to the call

        """
        pass

    @abstractmethod
    def SwitchBeep(self, On: bool, *, metadata: Dict[FullyQualifiedIdentifier, Any]) -> SwitchBeep_Responses:
        """
        Switch the scan indicator beep on or off


        :param On: If true, the scanner will beep on every scanned code

        :param metadata: The SiLA Client Metadata attached to the call

        """
        pass

    @abstractmethod
    def ScanContinuously(
        self,
        *,
        metadata: Dict[FullyQualifiedIdentifier, Any],
        instance: ObservableCommandInstanceWithIntermediateResponses[ScanContinuously_IntermediateResponses],
    ) -> ScanContinuously_Responses:
        """
        Enable continuous scanning mode


        :param metadata: The SiLA Client Metadata attached to the call
        :param instance: The command instance, enabling sending status updates to subscribed clients

        """
        pass
