from __future__ import annotations

from typing import Iterable, Optional

from barcodescanner_types import (
    ScanContinuously_IntermediateResponses,
    ScanContinuously_Responses,
    ScanForCode_Responses,
    StopScanning_Responses,
    SwitchBeep_Responses,
    SwitchTargetLight_Responses,
    SwitchWhiteLight_Responses,
)
from sila2.client import ClientMetadataInstance, ClientObservableCommandInstance, ClientObservableProperty

class BarcodeScannerClient:
    """
    Barcode scanner
    """

    CurrentMode: ClientObservableProperty[str]
    """
    The current scanning mode
    """

    WhiteLightEnabled: ClientObservableProperty[bool]
    """
    True if the white light is enabled during scanning
    """

    TargetLightEnabled: ClientObservableProperty[bool]
    """
    True if the red target light is enabled during scanning
    """

    BeepEnabled: ClientObservableProperty[bool]
    """
    True if the scan indicator beep is enabled during scanning
    """
    def ScanForCode(
        self, Timeout: float, *, metadata: Optional[Iterable[ClientMetadataInstance]] = None
    ) -> ScanForCode_Responses:
        """
        Scan for a barcode
        """
        ...
    def StopScanning(self, *, metadata: Optional[Iterable[ClientMetadataInstance]] = None) -> StopScanning_Responses:
        """
        Stop current scan
        """
        ...
    def SwitchWhiteLight(
        self, On: bool, *, metadata: Optional[Iterable[ClientMetadataInstance]] = None
    ) -> SwitchWhiteLight_Responses:
        """
        Switch the white light on or off
        """
        ...
    def SwitchTargetLight(
        self, On: bool, *, metadata: Optional[Iterable[ClientMetadataInstance]] = None
    ) -> SwitchTargetLight_Responses:
        """
        Switch the red target light on or off
        """
        ...
    def SwitchBeep(
        self, On: bool, *, metadata: Optional[Iterable[ClientMetadataInstance]] = None
    ) -> SwitchBeep_Responses:
        """
        Switch the scan indicator beep on or off
        """
        ...
    def ScanContinuously(
        self, *, metadata: Optional[Iterable[ClientMetadataInstance]] = None
    ) -> ClientObservableCommandInstance[ScanContinuously_IntermediateResponses, ScanContinuously_Responses]:
        """
        Enable continuous scanning mode
        """
        ...
