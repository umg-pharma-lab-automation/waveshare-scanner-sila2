from __future__ import annotations

from typing import NamedTuple


class ScanForCode_Responses(NamedTuple):

    Barcode: str
    """
    The scanned code
    """


class StopScanning_Responses(NamedTuple):

    pass


class SwitchWhiteLight_Responses(NamedTuple):

    pass


class SwitchTargetLight_Responses(NamedTuple):

    pass


class SwitchBeep_Responses(NamedTuple):

    pass


class ScanContinuously_Responses(NamedTuple):

    pass


class ScanContinuously_IntermediateResponses(NamedTuple):

    Barcode: str
    """
    A scanned code
    """
