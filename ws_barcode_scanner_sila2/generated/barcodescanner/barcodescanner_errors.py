from __future__ import annotations

from typing import Optional

from sila2.framework.errors.defined_execution_error import DefinedExecutionError

from .barcodescanner_feature import BarcodeScannerFeature


class ScanTimedOut(DefinedExecutionError):
    def __init__(self, message: Optional[str] = None):
        if message is None:
            message = "No code was found during the given timeframe"
        super().__init__(BarcodeScannerFeature.defined_execution_errors["ScanTimedOut"], message=message)


class Busy(DefinedExecutionError):
    def __init__(self, message: Optional[str] = None):
        if message is None:
            message = "Cannot enable continuous scanning while scanning for a single code"
        super().__init__(BarcodeScannerFeature.defined_execution_errors["Busy"], message=message)


class Cancelled(DefinedExecutionError):
    def __init__(self, message: Optional[str] = None):
        if message is None:
            message = "Scan was cancelled"
        super().__init__(BarcodeScannerFeature.defined_execution_errors["Cancelled"], message=message)


class NotScanning(DefinedExecutionError):
    def __init__(self, message: Optional[str] = None):
        if message is None:
            message = "Device is not scanning"
        super().__init__(BarcodeScannerFeature.defined_execution_errors["NotScanning"], message=message)
