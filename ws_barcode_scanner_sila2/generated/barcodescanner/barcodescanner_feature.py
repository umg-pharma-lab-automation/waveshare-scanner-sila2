from os.path import dirname, join

from sila2.framework import Feature

BarcodeScannerFeature = Feature(open(join(dirname(__file__), "BarcodeScanner.sila.xml")).read())
